import { Component, OnInit } from '@angular/core';

import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    currentUser;
    currentUsername: string;

    constructor(private authService: AuthService) { }

    ngOnInit() {
        this.currentUser = localStorage.getItem('currentUser');
        this.currentUsername = localStorage.getItem('currentUsername');
    }

    onLogout() {
        this.authService.logout();
    }

}
