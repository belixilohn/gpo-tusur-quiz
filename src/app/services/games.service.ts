import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';

import { Game } from '../models/game';

import { Subject } from 'rxjs/Subject';

@Injectable()
export class GamesService {

    gameAdded$ = new Subject<Game>();
    selectedGame$ = new Subject<Game>();

    constructor(private http: Http) { }

    getGames() {
        return this.http.get(AppConfig.apiRootUrl + 'api/games/', this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    getGame(id: number) {
        return this.http.get(AppConfig.apiRootUrl + 'api/games/' + id + '/', this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    addGame(game: Game) {
        return this.http.post(AppConfig.apiRootUrl + 'api/games/', game, this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    deleteGame(game: Game) {
        this.http.delete(AppConfig.apiRootUrl + 'api/games/' + game.id + '/', this.jwt()).subscribe();
    }

    getGameResults(gameId: number) {
        return this.http.get(AppConfig.apiRootUrl + 'api/games/' + gameId + '/results/', this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    saveGameResults(selectedGameId: number, answer_correctness) {
        return this.http.post(AppConfig.apiRootUrl + 'api/games/' + selectedGameId + '/results/', answer_correctness, this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    private jwt() {
        const headers = new Headers({ 'Authorization': 'Token ' + JSON.parse(localStorage.getItem('currentUser')).token });
        return new RequestOptions({ headers: headers });
    }

}
