import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions} from '@angular/http';

import { AppConfig } from '../app.config';

@Injectable()
export class RegistrationService {

    constructor(private http: Http) { }

    register(user) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(AppConfig.apiRootUrl + 'api/register/', JSON.stringify(user), options)
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }
}
