import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';

import { Question } from '../models/question';

import { Subject } from 'rxjs/Subject';

@Injectable()
export class QuestionsService {

    nextQuestionIndex$ = new Subject<number>();
    nextQuestion$ = new Subject<Question>();

    constructor(private http: Http) { }

    getQuestions() {
        return this.http.get(AppConfig.apiRootUrl + 'api/questions/', this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    getQuizQuestions(id: number) {
        return this.http.get(AppConfig.apiRootUrl + 'api/game_templates/' + id + '/questions/', this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    addQuestion(question: Question) {
        return this.http.post(AppConfig.apiRootUrl + 'api/questions/', question, this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    deleteQuestion(question: Question) {
        this.http.delete(AppConfig.apiRootUrl + 'api/questions/' + question.id + '/', this.jwt()).subscribe();
    }

    private jwt() {
        const headers = new Headers({ 'Authorization': 'Token ' + JSON.parse(localStorage.getItem('currentUser')).token });
        return new RequestOptions({ headers: headers });
    }
}
