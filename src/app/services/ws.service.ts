import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import 'rxjs/add/observable/dom/webSocket';
import { AppConfig } from '../app.config';
// import { Message } from '../models/message';

@Injectable()
export class WsService {

    private socket$: WebSocketSubject<any>;

    connect() {
        return this.socket$ = Observable.webSocket(AppConfig.wsUrl);
    }

    sendMsg(msg: any) {
        // console.log('Отправил ', JSON.stringify(msg));
        this.socket$.next(JSON.stringify(msg));
    }

}
