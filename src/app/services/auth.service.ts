import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { AppConfig } from '../app.config';

import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

    constructor(private http: Http) { }

    login(username: string, password: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(
            AppConfig.apiRootUrl + 'api/token/', JSON.stringify({ username: username, password: password }), { headers: headers })
            .map(response => {
                // login successful if there's a jwt token in the response
                const user = response.json();
                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    localStorage.setItem('currentUsername', username);
                }
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentUsername');
    }

}
