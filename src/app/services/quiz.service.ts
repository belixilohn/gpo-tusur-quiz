import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';

import { Quiz } from '../models/quiz';

@Injectable()
export class QuizService {

    constructor(private http: Http) { }

    getQuizzes() {
        return this.http.get(AppConfig.apiRootUrl + 'api/game_templates/', this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    getQuiz(id: number) {
        return this.http.get(AppConfig.apiRootUrl + 'api/game_templates/' + id + '/', this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    addQuiz(quiz: Quiz) {
        return this.http.post(AppConfig.apiRootUrl + 'api/game_templates/', quiz, this.jwt())
            .map((response: Response) => {
                const data = response.json();
                return data;
            });
    }

    deleteQuiz(quiz: Quiz) {
        this.http.delete(AppConfig.apiRootUrl + 'api/game_templates/' + quiz.id + '/', this.jwt()).subscribe();
    }

    private jwt() {
        const headers = new Headers({ 'Authorization': 'Token ' + JSON.parse(localStorage.getItem('currentUser')).token });
        return new RequestOptions({ headers: headers });
    }

}
