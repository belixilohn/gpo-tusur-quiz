import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AlertService {

    private subject = new Subject<any>();
    private keepAfterNavigationChange = false;

    constructor(private router: Router) {
        // clear alert message on route change
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    this.keepAfterNavigationChange = false;
                } else {
                    // clear alert
                    this.subject.next();
                }
            }
        });
    }

    success(message: Response, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    }

    error(message: any, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        const errorText = message.json();
        // status === 400
        if (errorText.non_field_errors) {
            if (errorText.non_field_errors[0] === 'Unable to log in with provided credentials.') {
                this.subject.next({text: 'Неправильное имя пользователя или пароль'});
            } else {
                this.subject.next({text: errorText.non_field_errors});
            }
        }
        if (errorText.email) {
            this.subject.next({text: errorText.email});
        }
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

}
