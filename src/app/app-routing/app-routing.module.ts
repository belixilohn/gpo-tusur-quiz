import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { RegistrationComponent } from '../registration/registration.component';
import { ActiveGameComponent } from '../active-game/active-game.component';
import { NotFoundComponent } from '../not-found/not-found.component';
import { CreateQuizComponent } from '../home/create-quiz/create-quiz.component';
import { ResultsComponent } from '../results/results.component';

import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegistrationComponent },
    { path: 'games/:gameId', component: ActiveGameComponent, canActivate: [AuthGuard] },
    { path: 'results/:gameId', component: ResultsComponent, canActivate: [AuthGuard] },
    { path: 'create', component: CreateQuizComponent, canActivate: [AuthGuard] },
    { path: '**', redirectTo: 'not-found' },
    { path: 'not-found', component: NotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
