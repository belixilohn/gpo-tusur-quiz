import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { GamesService } from '../services/games.service';
import { QuestionsService } from '../services/questions.service';
import { WsService } from '../services/ws.service';

import { Game } from '../models/game';
import { Question } from '../models/question';
import { Player } from '../models/player';

import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-active-game',
    templateUrl: './active-game.component.html',
    styleUrls: ['./active-game.component.css'],
})
export class ActiveGameComponent implements OnInit, OnDestroy {

    wsSubscription: Subscription;
    indexSubscription: Subscription;
    questionSubscription: Subscription;

    nextQuestionIndex: number;
    nextQuestion: Question;
    activeQuestionIndex: number;
    activeQuestion: Question;
    isPlaying = false;

    activeGameId: number;
    questions: Question[];
    players: Player[];

    firstQuestionDelivered = false;

    constructor(private activatedRoute: ActivatedRoute,
                private gamesService: GamesService,
                private router: Router,
                private questionsService: QuestionsService,
                private wsService: WsService) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            this.activeGameId = +params['gameId'];
        });
        this.gamesService.getGame(this.activeGameId).subscribe((game: Game) => {
            this.players = game.player_set;
            this.questionsService.getQuizQuestions(game.game_template).subscribe((questions: Question[]) => {
                this.questions = questions;
            });
            this.players.forEach((player: any) => player.answered = false);
        }, err => {
            this.router.navigate(['/not-found']);
        });
        this.indexSubscription = this.questionsService.nextQuestionIndex$.subscribe((index: number) => {
            this.activeQuestionIndex = index;
        });
        this.questionSubscription = this.questionsService.nextQuestion$.subscribe((question: Question) => {
            this.activeQuestion = question;
        });
        this.wsSubscription = this.wsService.connect().subscribe(
            (msg: any) => {
                console.log('Сервер уведомлений прислал сообщение: ', msg);
                if (msg.type === 'next_answer') {
                    for (const player of this.players) {
                        if (player.username === msg.payload.author) {
                            player['answered'] = true;
                            break;
                        }
                    }
                    console.log(this.players);
                }
                if (msg.type === 'game_over') {
                    this.router.navigate(['/']);
                }
            },
            err => console.log('Ошибка от сервера уведомлений: ', err),
            () => console.log('complete')
        );
    }

    onQuestionSelect(index: number, question: Question) {
        if (this.isPlaying && this.firstQuestionDelivered && this.activeQuestion !== question) {
            this.players.forEach((player: any) => player.answered = false);
            this.questionsService.nextQuestionIndex$.next(index);
            this.questionsService.nextQuestion$.next(question);
            this.wsService.sendMsg({
                type: 'next_question',
                payload: {
                    id: this.activeQuestion.id
                }
            });
        }
    }

    onStart() {
        this.wsService.sendMsg({
            type: 'auth',
            payload: {
                token: JSON.parse(localStorage.getItem('currentUser')).token,
                game_id: this.activeGameId
            }
        });
        this.isPlaying = true;
        this.activeQuestionIndex = 0;
        this.activeQuestion = this.questions[this.activeQuestionIndex];
    }


    sendFirstQuestion() {
        this.wsService.sendMsg({
            type: 'next_question',
            payload: {
                id: this.activeQuestion.id
            }
        });
        this.firstQuestionDelivered = true;
    }

    onNext() {
        this.players.forEach((player: any) => player.answered = false);
        this.questionsService.nextQuestionIndex$.next(++this.activeQuestionIndex);
        this.questionsService.nextQuestion$.next(this.questions[this.activeQuestionIndex]);
        this.wsService.sendMsg({
            type: 'next_question',
            payload: {
                id: this.activeQuestion.id
            }
        });
    }

    onFinish() {
        this.wsService.sendMsg({
            type: 'game_over',
            payload: {
                game: this.activeGameId
            }
        });
        // setTimeout((router: Router) => {
        //     this.router.navigate(['/']);
        // }, 1000);
    }

    ngOnDestroy() {
        this.wsSubscription.unsubscribe();
        this.indexSubscription.unsubscribe();
        this.questionSubscription.unsubscribe();
    }

}
