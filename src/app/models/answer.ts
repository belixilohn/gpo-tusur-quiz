export class Answer {
    id: number;
    author: string;
    text: string;
    question: number;
    status: string;
}
