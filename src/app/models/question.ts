import { Answer } from './answer';

export class Question {
    id: number;
    text: string;
    right_answer: string;
    answers: Answer[]; // моё
}
