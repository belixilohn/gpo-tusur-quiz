import { Question } from './question';
import { Answer } from './answer';
import { Player } from './player';
import { Quiz } from './quiz';

export class Game  {
    id: number;
    name: string;
    is_played: boolean;
    link: string;
    game_template: number;
    answer_set: Answer[];
    player_set: Player[];
}
