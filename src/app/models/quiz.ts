import { Question } from './question';

export class Quiz {
    id: number;
    name: string;
    question_set: number[];
    constructor () {
        this.question_set = [];
    }
}
