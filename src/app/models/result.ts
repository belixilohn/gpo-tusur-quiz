import { Question } from './question';
import { Answer } from './answer';
import { Player } from './player';

export class Result {
    question_set: Question[];
    answer_set: Answer[];
    player_set: Player[];
    players_score: string;
}
