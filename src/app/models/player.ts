import { Answer } from './answer';

export class Player {
    user: number;
    username: string;
    email: string;
    game: number;
    answer: Answer; // моё
    right_answers: number; // моё
}
