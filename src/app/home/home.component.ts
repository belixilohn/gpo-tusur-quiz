import { Component, OnInit, OnDestroy, trigger, style, animate, transition } from '@angular/core';
import { Router } from '@angular/router';
import { CreateGameModalComponent } from './create-game-modal/create-game-modal.component';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Subscription } from 'rxjs/Subscription';

import { QuizService } from '../services/quiz.service';
import { GamesService } from '../services/games.service';
import { BsModalService } from 'ngx-bootstrap/modal';

import { Quiz } from '../models/quiz';
import { Game } from '../models/game';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    animations: [
        trigger('deleteQuiz', [
            transition('* => void', [
                animate(450, style({ transform: 'translateX(-500px) scale(0)', opacity: 0 }))
            ])
        ]),
        trigger('deleteGame', [
            transition('* => void', [
                animate(450, style({ transform: 'translateX(500px) scale(0)', opacity: 0 }))
            ])
        ])
    ]
})
export class HomeComponent implements OnInit, OnDestroy {

    quizzes: Quiz[];
    currentGames: Game[] = [];
    history: Game[] = [];
    subscription: Subscription;
    createGameModalRef: BsModalRef;

    constructor(private quizService: QuizService,
                private gamesService: GamesService,
                private modalService: BsModalService,
                private router: Router) { }

    ngOnInit() {
        this.quizService.getQuizzes().subscribe((quizzes: Quiz[]) => {
            this.quizzes = quizzes;
        },
        error => {
            console.log(error.json());
            this.router.navigate(['/login']);
        });
        this.gamesService.getGames().subscribe((games: Game[]) => {
            games.forEach(game => {
                if (!game.is_played) {
                    this.currentGames.push(game);
                } else {
                    this.history.push(game);
                }
            });
        });
        this.subscription = this.gamesService.gameAdded$.subscribe((game: Game) => {
            this.currentGames.push(game);
        });
    }

    onCreateGame(quiz: Quiz) {
        this.createGameModalRef = this.modalService.show(CreateGameModalComponent);
        this.createGameModalRef.content.selectedQuiz = quiz;
    }

    onDeleteQuiz(quiz: Quiz, index: number) {
        this.quizService.deleteQuiz(quiz);
        this.quizzes.splice(index, 1);
    }

    onDeleteGame(game: Game, index: number) {
        this.gamesService.deleteGame(game);
        this.currentGames.splice(index, 1);
    }

    onSelectGame(game: Game) {
        this.gamesService.selectedGame$.next(game);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
