import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';


import { QuizService } from '../../services/quiz.service';
import { GamesService } from '../../services/games.service';

import { Quiz } from '../../models/quiz';
import { Game } from '../../models/game';

@Component({
    selector: 'app-create-game-modal',
    templateUrl: './create-game-modal.component.html',
    styleUrls: ['./create-game-modal.component.css']
})
export class CreateGameModalComponent {

    selectedQuiz: Quiz;
    newGame = new Game();
    nameError = false;
    linkError = false;
    lastTriedName: string;
    lastTriedLink: string;
    nameErrorText: string;
    linkErrorText: string;

    constructor(public createGameModalRef: BsModalRef, private quizService: QuizService, private gamesService: GamesService) { }

    onSaveGame(f: NgForm) {
        if (f.valid) {
            this.newGame.game_template = this.selectedQuiz.id;
            this.gamesService.addGame(this.newGame).subscribe(
                (game: Game) => {
                    this.gamesService.gameAdded$.next(game);
                    this.createGameModalRef.hide();
                },
                (error: any) => {
                    const message: any = error.json();
                    if (error.status === 400 && message.non_field_errors[0]) {
                        if (message.non_field_errors[0] === 'Game') {
                            this.nameError = true;
                            this.lastTriedName = this.newGame.name;
                            this.nameErrorText = `Игра с названием "${this.newGame.name}" уже существует`;
                        }
                        if (message.non_field_errors[0] === 'Link') {
                            this.linkError = true;
                            this.lastTriedLink = this.newGame.link;
                            this.linkErrorText = `Игра со ссылкой "${this.newGame.link}" уже существует`;
                        }
                    } else {
                        console.log(message, 'ошибка во время сохранения игры');
                    }
                });
        }
    }

}
