import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { QuestionsService } from '../../services/questions.service';
import { DragulaService } from 'ng2-dragula';
import { QuizService } from '../../services/quiz.service';

import { Quiz } from '../../models/quiz';
import { Question } from '../../models/question';

@Component({
    selector: 'app-create-quiz',
    templateUrl: './create-quiz.component.html',
    styleUrls: ['./create-quiz.component.css']
})
export class CreateQuizComponent implements OnInit, OnDestroy {

    allQuestions: Question[] = [];

    questionsToSend: Question[] = [];

    newQuiz = new Quiz();
    lastTriedName: string;

    isAdding = false;
    isQuestion = false;
    isAnswer = false;
    creationError = false;
    errorText: string;

    constructor(private questionsService: QuestionsService,
        private dragulaService: DragulaService,
        private quizService: QuizService,
        private router: Router) { }

    ngOnInit() {
        this.questionsService.getQuestions().subscribe((questions: Question[]) => {
            this.allQuestions = questions;
        });
    }

    onAdd() {
        this.isAdding = true;
        this.isAnswer = true;
        this.isQuestion = true;
    }

    onSaveQuestion(newQuestionText: string, rightAnswerText: string) {
        this.isQuestion = newQuestionText === '' ? false : true;
        this.isAnswer = rightAnswerText === '' ? false : true;
        if (this.isQuestion && this.isAnswer) {
            const questionToAdd = new Question();
            questionToAdd.text = newQuestionText;
            questionToAdd.right_answer = rightAnswerText;
            this.questionsService.addQuestion(questionToAdd).subscribe(
                (question: Question) => {
                    this.allQuestions.push(question);
                },
                err => {
                    console.log(err, 'ошибка во время сохранения вопроса');
                });
            this.isAdding = false;
        }
    }

    onSaveQuiz() {
        this.questionsToSend.forEach((element, index) => {
            this.newQuiz.question_set[index] = element.id;
        });
        this.quizService.addQuiz(this.newQuiz).subscribe(
            (quiz: Quiz) => {
                this.router.navigate(['/']);
            },
            (error: Response) => {
                const message: any = error.json();
                if (error.status === 400 && message.name) {
                    this.lastTriedName = this.newQuiz.name;
                    this.errorText = `Викторина с названием "${this.newQuiz.name}" уже существует`;
                    this.creationError = true;
                } else {
                    console.log(message, 'ошибка во время сохранения викторины');
                }
            });
    }

    onClear() {
        this.questionsToSend.length = 0;
        this.questionsService.getQuestions().subscribe((questions: Question[]) => {
            this.allQuestions = questions;
        });
    }

    onDeleteQuestion(question: Question, index: number) {
        if (confirm('Вы уверены, что хотите удалить вопрос?')) {
            this.questionsService.deleteQuestion(question);
            this.allQuestions.splice(index, 1);
        }
    }

    ngOnDestroy() {
        this.dragulaService.destroy('my-bag');
    }

}
