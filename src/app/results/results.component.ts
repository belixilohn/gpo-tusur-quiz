import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import { QuestionsService } from '../services/questions.service';
import { GamesService } from '../services/games.service';

import { Question } from '../models/question';
import { Game } from '../models/game';
import { Answer } from '../models/answer';
import { Player } from '../models/player';
import { Result } from '../models/result';
import { $ } from 'protractor';


@Component({
    selector: 'app-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnDestroy {

    selectedGameId: number;
    indexSubscription: Subscription;
    questionSubscription: Subscription;
    activeQuestionIndex: number;
    activeQuestion: Question;
    questionSelected = false;
    correctnessToSend = {};
    result = new Result();
    loading = false;
    hasChanges = false;


    constructor(private activatedRoute: ActivatedRoute,
        private gamesService: GamesService,
        private router: Router,
        private questionsService: QuestionsService) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            this.selectedGameId = +params['gameId'];
        });
        this.gamesService.getGameResults(this.selectedGameId).subscribe(
            (results: Result[]) => {
                this.result = results[0];
                this.result.player_set.forEach((player: Player) => {
                   player.right_answers = 0;
                });
                this.result.question_set.forEach((question: Question) => {
                    question.answers = this.result.answer_set.filter((answer: Answer) => {
                        return answer.question === question.id;
                    });
                    question.answers.forEach((answer: Answer) => {
                        if (answer.status === 'right') {
                            this.result.player_set.find((player: Player) => {
                                return answer.author === player.username;
                            }).right_answers++;
                        }
                    });
                });
                this.correctnessToSend['answer_correctness'] = JSON.parse(JSON.stringify(this.result.answer_set));
                this.correctnessToSend['answer_correctness'].forEach(obj => {
                    obj.answer_id = obj.id;
                    delete obj.author;
                    delete obj.text;
                    delete obj.duration;
                    delete obj.question;
                });
                this.correctnessToSend['answer_correctness'].forEach(obj => {
                    delete obj.id;
                });
            },
            err => {
                this.router.navigate(['/not-found']);
            });
        this.indexSubscription = this.questionsService.nextQuestionIndex$.subscribe((index: number) => {
            this.activeQuestionIndex = index;
        });
        this.questionSubscription = this.questionsService.nextQuestion$.subscribe((question: Question) => {
            this.activeQuestion = question;
        });
    }

    onQuestionSelect(i, question) {
        this.questionsService.nextQuestion$.next(question);
        this.questionsService.nextQuestionIndex$.next(i);
        this.questionSelected = true;
        this.result.player_set.forEach((player: Player) => {
            const currentQuestionAnswer = this.result.answer_set.find((answer: Answer) => {
                return answer.author === player.username && answer.question === this.activeQuestion.id;
            });
            if (currentQuestionAnswer) {
                player.answer = currentQuestionAnswer;
            } else {
                player.answer = null;
            }
        });
    }

    onVerify(answer: Answer, statusElem: HTMLElement) {
        this.hasChanges = true;
        let clickedStatus;
        const player = this.result.player_set.find((pl: Player) => pl.username === answer.author);
        if (statusElem.classList.contains('fa-check')) {
            clickedStatus = 'right';
            if (player.answer.status === 'wrong' ||
                player.answer.status === 'unverified') {
                player.right_answers++;
            }
        } else {
            clickedStatus = 'wrong';
            if (player.answer.status === 'right') {
                player.right_answers--;
            }
        }
        if (this.correctnessToSend['answer_correctness'].find(obj => obj.answer_id === answer.id && obj.status !== clickedStatus)) {
            this.correctnessToSend['answer_correctness'].find(obj => {
                return obj.answer_id === answer.id && obj.status !== clickedStatus;
            }).status = clickedStatus;
        }
        this.result.answer_set.find((answ: Answer) => {
            return answ.id === answer.id;
        }).status = clickedStatus;
    }

    onSave() {
        this.loading = true;
        this.gamesService.saveGameResults(this.selectedGameId, this.correctnessToSend).subscribe((data) => {
            console.log(data);
            this.loading = false;
        },
        (error: Response) => {
            console.log(error);
        });
    }

    ngOnDestroy() {
        this.indexSubscription.unsubscribe();
        this.questionSubscription.unsubscribe();
    }

}
