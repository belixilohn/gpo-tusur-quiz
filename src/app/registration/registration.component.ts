import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService } from '../services/alert.service';
import { RegistrationService } from '../services/registration.service';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})

export class RegistrationComponent {
    model: any = {};
    loading = false;

    constructor(private router: Router, private alertService: AlertService, private registrationService: RegistrationService) { }

    register() {
        this.loading = true;
        this.registrationService.register(this.model).subscribe(
            data => {
                this.router.navigate(['/login']);
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
    }
}
