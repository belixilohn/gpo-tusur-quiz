import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragulaModule } from 'ng2-dragula';
import { ModalModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
// import { BsDatepickerModule } from 'ngx-bootstrap';

import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { CreateQuizComponent } from './home/create-quiz/create-quiz.component';
import { CreateGameModalComponent } from './home/create-game-modal/create-game-modal.component';
import { ActiveGameComponent } from './active-game/active-game.component';
import { AlertComponent } from './alert/alert.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { AlertService } from './services/alert.service';
import { AuthService } from './services/auth.service';
import { WsService } from './services/ws.service';
import { RegistrationService } from './services/registration.service';
import { QuizService } from './services/quiz.service';
import { QuestionsService } from './services/questions.service';
import { GamesService } from './services/games.service';

import { AuthGuard } from './guards/auth.guard';
import { ResultsComponent } from './results/results.component';

// import { defineLocale } from 'ngx-bootstrap/bs-moment';
// import { ru } from 'ngx-bootstrap/locale';
// defineLocale('ru', ru);

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        CreateQuizComponent,
        LoginComponent,
        RegistrationComponent,
        AlertComponent,
        ActiveGameComponent,
        NotFoundComponent,
        HomeComponent,
        CreateGameModalComponent,
        ResultsComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        DragulaModule,
        ModalModule.forRoot(),
        AccordionModule.forRoot(),
        TabsModule.forRoot(),
        // BsDatepickerModule.forRoot()
    ],
    providers: [
        AlertService,
        AuthService,
        WsService,
        RegistrationService,
        QuizService,
        QuestionsService,
        GamesService,
        AuthGuard
    ],
    entryComponents: [
        CreateGameModalComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
